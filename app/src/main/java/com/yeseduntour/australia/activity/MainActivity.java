package com.yeseduntour.australia.activity;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.yeseduntour.australia.R;

public class MainActivity extends AppCompatActivity {

    LinearLayout blog_web;
    RelativeLayout main_view, sale_view;

    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        TextView sale_text = (TextView)findViewById(R.id.sale_text);

        blog_web = (LinearLayout)findViewById(R.id.blog_web);
        main_view = (RelativeLayout)findViewById(R.id.main_view);
        sale_view = (RelativeLayout)findViewById(R.id.sale_view);


        blog_web.setOnClickListener(btnListener);
        main_view.setOnClickListener(btnListener);
        sale_view.setOnClickListener(btnListener);

        //text color
        SpannableStringBuilder text_color = new SpannableStringBuilder(sale_text.getText());
        text_color.setSpan(new ForegroundColorSpan(Color.parseColor("#FFA2FF")), 10, 14, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        sale_text.setText(text_color);

    }
    View.OnClickListener btnListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent;
            switch (v.getId()){
                case R.id.main_view:
                    main_view.setVisibility(View.GONE);
                    sale_view.setVisibility(View.VISIBLE);
                    break;
                case R.id.sale_view:
                    main_view.setVisibility(View.VISIBLE);
                    sale_view.setVisibility(View.GONE);
                    break;
                case R.id.blog_web:
                    intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://bitly.kr/bYGB2b"));
                    startActivity(intent);
                    break;
            }
        }
    };


}
